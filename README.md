antiSMASH allows the rapid genome-wide identification, annotation and analysis
of secondary metabolite biosynthesis gene clusters in bacterial and fungal
genomes. It integrates and cross-links with a large number of in silico
secondary metabolite analysis tools that have been published earlier.

antiSMASH is powered by several open source tools: NCBI BLAST+,HMMer 3, Muscle
3, Glimmer 3, FastTree, TreeGraph 2, Indigo-depict, PySVG and JQuery SVG.

The database additionally requires PostgreSQL and psycopg2

The development of antiSMASH was started as a collaboration of the Department
of Microbial Physiology and Groningen Bioinformatics Centre of the University
of Groningen, the Department of Microbiology of the University of Tübingen, and
the Department of Bioengineering and Therapeutic Sciences at the University of
California, San Francisco.
With the move of the PI's develoment continues now at the Manchester Institute
of Biotechnology, the Max Planck Institute for Marine Microbiology Bremen and
The Novo Nordisk Foundation Center for Biosustainability in Hørsholm

antiSMASH develoment was/is supported by the GenBiotics program of the Dutch Technology
Foundation (STW), which is the applied-science division of The Netherlands
Organisation for Scientific Research (NWO) and the Technology Programme of the
Ministry of Economic Affairs (grant STW 10463), GenBioCom program
of the German Ministry of Education and Research (BMBF) grant 0315585A, the
German Center for Infection Research (DZIF) and the Novo Nordisk Foundation.

Publications:
Kai Blin, Marnix H. Medema, Daniyal Kazempour, Michael A. Fischbach, Rainer
Breitling, Eriko Takano, & Tilmann Weber (2013): antiSMASH 2.0 — a versatile
platform for genome mining of secondary metabolite producers. Nucleic Acids
Research 41: W204-W212.

Marnix H. Medema, Kai Blin, Peter Cimermancic, Victor de Jager, Piotr
Zakrzewski, Michael A. Fischbach, Tilmann Weber, Rainer Breitling & Eriko
Takano (2011). antiSMASH: Rapid identification, annotation and analysis of
secondary metabolite biosynthesis gene clusters. Nucleic Acids Research 39:
W339-W346.

Database setup guide

1. Install PostgreSQL according to your operation system's specifications
2. Install python db and psycopg2 module
	pip install db
	pip install psycopg2
	
2. Setup BioSQL database
	Owner: biosql
	Database name: biosql
	Password (standard): biosql # should be changed in config/default.cfg
	
	2a: Follow the guide http://biopython.org/wiki/BioSQL; load taxonomy using the supplied perl script (requires Bioperl/Bioperl-DB)
	2b: Alternatively load biosqldump.sql 
	(download from https://bitbucket.org/tilmweber/antismash-db_private/downloads); this include BioSQL with pre-loaded NCBI taxonomy database
	
3. If you wish to use the nosetest framework also for testing the database modules, setup a second BioSQL database. This database is used only for the test scripts
	Owner: biosql
	Database name: antiSMASHnosetest
	Password: biosql
	
# vim: set fileencoding=utf-8 :
#

#
# Copyright (C) 2014 Tilmann Weber
# The Novo Nordisk Foundation Center for Biosustainability
# Technical University of Denmark
# Section: Metabolic Engineering for Natural Compounds / New Bioactive Compounds
#
# Copyright (C) 2014 Hyun Uk Kim
# The Novo Nordisk Foundation Center for Biosustainability
# Technical University of Denmark
# Section: Metabolic Engineering for Natural Compounds / New Bioactive Compounds
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
"""
Automodel core pipeline script
"""

from prunPhase import *
from augPhase import *
from argparse import Namespace
import cobra
from cobra.io.sbml import write_cobra_model_to_sbml_file
import copy
import pickle
#import urllib2
import os
import logging

from antismash import utils

def run_automodel(seq_records, options):


    #List of input (static) files as pickles
    ###################################################################
    #from "independentModule_"
    
    #For model pruning phase
    #Choose "eco" or "sco"
    #root = os.path.dirname(utils.get_full_path('__file__', 'input1'))
    if not cobra.__version__ == "0.2.1":
        logging.error("The modeling pipeline is only compatible wit COBRApy version 0.2.1; your insstalled version is %s" % \
                      cobra.__version__)
        return False
    
    root = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'input1'
    temp_fasta = options.metabolicmodeldir
    
    #root, temp_fasta = get_temp_fasta(orgName)
    logging.debug("[metabolicmodel] set up model dir as %s and output dir as %s" % (root, temp_fasta))
    
    model = pickle.load(open(root+os.sep+options.modeling+os.sep+'model.p','rb'))
    tempModel_biggRxnid_locusTag_dict = pickle.load(open(root+os.sep+options.modeling+os.sep+'tempModel_biggRxnid_locusTag_dict.p','rb'))
    
    #For model augmentation  phase
    logging.debug( "loading pickle files of the parsed template model and its relevant genbank data..")
    kegg_mnxr_dict = pickle.load(open(root+os.sep+'kegg_mnxr_dict.p','rb'))
    mnxr_kegg_dict = pickle.load(open(root+os.sep+'mnxr_kegg_dict.p','rb'))
    
    mnxr_rxn_dict = pickle.load(open(root+os.sep+'mnxr_rxn_dict.p','rb'))
    
    bigg_mnxm_compound_dict = pickle.load(open(root+os.sep+'bigg_mnxm_compound_dict.p','rb'))
    mnxm_bigg_compound_dict = pickle.load(open(root+os.sep+'mnxm_bigg_compound_dict.p','rb'))
    kegg_mnxm_compound_dict = pickle.load(open(root+os.sep+'kegg_mnxm_compound_dict.p','rb'))
    mnxm_kegg_compound_dict = pickle.load( open(root+os.sep+'mnxm_kegg_compound_dict.p','rb'))
    
    mnxm_compoundInfo_dict = pickle.load(open(root+os.sep+'mnxm_compoundInfo_dict.p','rb'))
    
    templateModel_bigg_mnxr_dict = pickle.load(open(root+os.sep+'templateModel_bigg_mnxr_dict.p','rb'))
    ###################################################################
                               
    logging.debug("pruning phase starting..")
    ###################################################################
#     logging.debug("looking for a gbk file of a target genome..")
#     target_gbk = get_target_gbk()
    
    logging.debug("reading genbank file of the target genome.."    )
    targetGenome_locusTag_ec_dict, targetGenome_locusTag_prod_dict, target_fasta = get_targetGenomeInfo(seq_records, options)
    
    if len(targetGenome_locusTag_ec_dict) == 0:
        logging.error("Error: no EC_number in sequence record; skipping modeling")
        return False
    
#     logging.debug("\n", "looking for a fasta file of a target genome..", "\n")
#     target_fasta = get_target_fasta()
    
    logging.debug("generating a DB for the genes from the target genome..")
    make_blastDB(query_fasta=target_fasta, options=options)
    
    logging.debug("running BLASTP #1: genes in the target genome against genes in the template model..")
    run_blastp(target_fasta=options.metabolicmodeldir+os.sep+'targetGenome_locusTag_aaSeq.fa', \
               blastp_result=options.metabolicmodeldir+os.sep+'blastp_targetGenome_against_tempGenome.txt',\
               db_dir=root+os.sep+options.modeling+os.sep+'tempBlastDB', evalue=1e-30)
    
    logging.debug("running BLASTP #2: genes in the template model against genes in the target genome..")
    run_blastp(target_fasta=root+os.sep+options.modeling+os.sep+'tempModel_locusTag_aaSeq.fa', \
               blastp_result=options.metabolicmodeldir+os.sep+'blastp_tempGenome_against_targetGenome.txt',\
               db_dir = options.metabolicmodeldir+os.sep+'targetBlastDB', evalue=1e-30)
    
    logging.debug("parsing the results of BLASTP #1..")
    blastpResults_dict1 = parseBlaspResults(options.metabolicmodeldir+os.sep+'blastp_targetGenome_against_tempGenome.txt', \
                                            options.metabolicmodeldir+os.sep+'blastp_targetGenome_against_tempGenome_parsed.txt')
     
    logging.debug("parsing the results of BLASTP #2..")
    blastpResults_dict2 = parseBlaspResults(options.metabolicmodeldir+os.sep+'blastp_tempGenome_against_targetGenome.txt', \
                                            options.metabolicmodeldir+os.sep+'blastp_tempGenome_against_targetGenome_parsed.txt')
    
    logging.debug("selecting the best hits for BLASTP #1..")
    bestHits_dict1 = makeBestHits_dict(options.metabolicmodeldir+os.sep+'blastp_targetGenome_against_tempGenome_parsed.txt')
    
    logging.debug("selecting the best hits for BLASTP #2..")
    bestHits_dict2 = makeBestHits_dict(options.metabolicmodeldir+os.sep+'blastp_tempGenome_against_targetGenome_parsed.txt')
    
    logging.debug("selecting the bidirectional best hits..")
    targetBBH_list, temp_target_BBH_dict = getBBH(bestHits_dict1, bestHits_dict2)
    
    
    logging.debug("selecting genes that are not bidirectional best hits..")
    nonBBH_list = get_nonBBH(targetGenome_locusTag_ec_dict, targetBBH_list)
    ###################################################################
    
    ###################################################################
    logging.debug("labeling reactions with nonhomologous genes to remove from the template model..")
    rxnToRemove_dict = labelRxnToRemove(model, temp_target_BBH_dict, tempModel_biggRxnid_locusTag_dict)
    
    logging.debug("removing reactions with nonhomologous genes from the template model..")
    modelPruned, rxnToRemoveEssn_dict, rxnRemoved_dict, rxnRetained_dict = pruneModel(model, rxnToRemove_dict, options.automodel.solver)
    
    logging.debug("correcting GPR associations in the template model..")
    modelPrunedGPR = swap_locusTag_tempModel(modelPruned, temp_target_BBH_dict)
    ###################################################################
    
    
    logging.debug("augmentation phase starting..")
    ###################################################################
    #NOT USED AT THE MOMENT
    logging.debug("creacting dictionary files for the noBBH genes...")
    #locusTag_geneID_dict, geneID_locusTag_dict = make_locusTag_geneID_nonBBH(target_gbk, "genbank", nonBBH_list)
    ###################################################################
    
    ###################################################################
    logging.debug("creating various dictionary files for the nonBBH gene-associted reactions...")
    
    #NOT USED AT THE MOMENT
    #Four nested functions
    #def get_species_locusTag(ncbi_geneid):
    #def get_ECNumberList_from_locusTag(species_locusTag):
    #def get_rxnid_from_ECNumber(enzymeEC):
    #def get_rxnInfo_from_rxnid(rxnid):
    #rxnid_info_dict, rxnid_locusTag_dict = make_all_rxnInfo_fromKEGG(locusTag_geneID_dict)
    
    targetGenome_locusTag_ec_nonBBH_dict = get_targetGenome_locusTag_ec_nonBBH_dict(targetGenome_locusTag_ec_dict, nonBBH_list)
    
    #Two nested functions
    #def get_rxnid_from_ECNumber(enzymeEC):
    #def get_rxnInfo_from_rxnid(rxnid):
    rxnid_info_dict, rxnid_locusTag_dict = make_all_rxnInfo_fromRefSeq(targetGenome_locusTag_ec_nonBBH_dict, options)
    ###################################################################
    
    ###################################################################
    logging.debug("adding the nonBBH gene-associated reactions...")
    rxnid_to_add_list = check_existing_rxns(kegg_mnxr_dict, templateModel_bigg_mnxr_dict, rxnid_info_dict)
    
    mnxr_to_add_list = get_mnxr_using_kegg(rxnid_to_add_list, kegg_mnxr_dict)
    
    rxnid_mnxm_coeff_dict = extract_rxn_mnxm_coeff(mnxr_to_add_list, mnxr_rxn_dict, mnxm_bigg_compound_dict, mnxm_kegg_compound_dict, mnxr_kegg_dict)
    
    #One nested function
    #get_compoundInfo(compoundID)
    target_model = add_nonBBH_rxn(modelPrunedGPR, rxnid_info_dict, rxnid_mnxm_coeff_dict, rxnid_locusTag_dict, bigg_mnxm_compound_dict, kegg_mnxm_compound_dict, mnxm_compoundInfo_dict, targetGenome_locusTag_prod_dict)
    ###################################################################
    
    #Output on screen
    model = pickle.load(open(root+os.sep+options.modeling+os.sep+'model.p','rb'))
    logging.debug("Number of genes: %s / %s / %s" % (len(model.genes), len(modelPruned.genes), len(target_model.genes)))
    logging.debug("Number of reactions: %s / %s / %s" % (len(model.reactions), len(modelPruned.reactions), len(target_model.reactions)))
    logging.debug("Number of metabolites: %s / %s / %s" % (len(model.metabolites), len(modelPruned.metabolites), len(target_model.metabolites)))
    
#     #Output files
#     write_cobra_model_to_sbml_file(target_model, options.metabolicmodeldir+os.sep+'antiSMASH_model_with_template_%s.xml' %(options.modeling))
#     
#     fp1 = open(options.metabolicmodeldir+os.sep+'antiSMASH_model_with_template_%s_reactions.txt' % options.modeling, "w")
#     fp2 = open(options.metabolicmodeldir+os.sep+'antiSMASH_model_with_template_%s_metabolites.txt' % options.modeling, "w")
#     fp1.write("Reaction ID"+"\t"+"Reaction name"+"\t"+"Lower bound"+"\t"+"Reaction equation"+"\t"+"GPR"+"\t"+"Pathway"+"\n")
#     fp2.write("Metabolite ID"+"\t"+"Metabolite name"+"\t"+"Formula"+"\t"+"Compartment"+"\n")
#     
#     for j in range(len(target_model.reactions)):
#         rxn = target_model.reactions[j]
#         print >>fp1, '%s\t%s\t%s\t%s\t%s\t%s' %(rxn.id, rxn.name, rxn.lower_bound, rxn.reaction, rxn.gene_reaction_rule, rxn.subsystem)
#     
#     for i in range(len(target_model.metabolites)):
#         metab = target_model.metabolites[i]
#         print >>fp2, '%s\t%s\t%s\t%s' %(metab.id, metab.name, metab.formula, metab.compartment)
#     
#     fp1.close()
#     fp2.close()
#     
#     options.metabolicmodel = options.metabolicmodeldir+os.sep+'antiSMASH_model_with_template_%s.xml' %(options.modeling)
    
    # Set up extrarecord data structure within options, if not already set
    if not "extrarecord" in options:
        options.extrarecord = {}
        
    # store model data in seq_records[0]
    seq_record = seq_records[0]
    if not options.extrarecord.has_key(seq_record.id):
        options.extrarecord[seq_record.id] = utils.Storage()
    if not "extradata" in options.extrarecord[seq_record.id]:
        options.extrarecord[seq_record.id].extradata = {}
    
    # as the cobra model object does not provide an own serialization, let's try with pickle...
    options.extrarecord[seq_record.id].extradata["MetabolicModelDataObj"] = pickle.dumps(target_model)
    
    if options.extrarecord[seq_record.id].extradata.has_key('MetabolicModelDataObj'):
        logging.debug("Generate options.extrarecord entry")
    else:
        logging.warning("Could not generate options.extrarecord for %s", seq_record.id)
    
    return True

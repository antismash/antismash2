<!doctype html>
<html>
  <head>
    <title>antiSMASH results</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <meta charset="utf-8" />
  </head>
  <body>
    <div id="header">
      <div class="top-header">
        <img class="antismash-logo" src="images/antismash.png" alt="antiSMASH">
        <span class="antismash-title"><a href="http://antismash.secondarymetabolites.org/">antibiotics & Secondary Metabolite Analysis SHell</a><br>
            <span class="white">Version <span id="antismash-version"></span></span>
        </span>
        <div id="icons">
          <a href="http://antismash.secondarymetabolites.org/"><img src="images/home.png" alt="home" title="Go to start page"></a>
          <a href="http://antismash.secondarymetabolites.org/help.html"><img src="images/help.png" alt="help" title="Get help using antiSMASH"></a>
          <a href="http://antismash.secondarymetabolites.org/about.html"><img src="images/about.png" alt="about" title="About antiSMASH"></a>
          <a href="#" id="download"><img src="images/download.png" alt="download" title="Download results"></a>
          <div id="downloadmenu">
            <ul id="downloadoptions">
            </ul>
          </div>
        </div>
      </div>
      <div id="buttons">
        <span id="cluster-type">Select Gene Cluster:</span>
        <ul id="clusterbuttons">
          <li class="clbutton"><a href="#">Overview</a></li>
        </ul>
      </div>
    </div>

    <!-- overview page -->
    <div class="page" id="overview">
      <h3>Identified secondary metabolite clusters</h3>
      <table id="cluster-overview">
        <thead>
          <tr>
            <th>Cluster</th>
            <th>Type</th>
            <th>From</th>
            <th>To</th>
            <th>Most similar known cluster</th>
            <th>MIBiG BGC-ID</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    
    <div id="footer">
      <div id="logos">
      	<table id="logo-table">
      		<tr>
      			<td>
        			<img src="images/tueblogo.gif">
        		</td>
        		<td>
        			<img src="images/ruglogo.gif">
        		</td>
        		<td>
        			<img src="images/ucsflogo.gif">
        		</td>
        		<td>
        			<img src="images/mpilogo.png">
        		</td>
        	</tr>
        	<tr>
        		<td>
        			<img src="images/uomlogo.jpg">
        		</td>
        		<td>
        			<img src="images/dziflogo.png">
        		</td>
        		<td>
        			<img src="images/cfb-logo.png">
        		</td>
        		<td>
        		</td>
        	</tr>
        </table>
      </div>
      <div id="copyright">
antiSMASH 2.0 - a versatile platform for genome mining of secondary metabolite producers.
Kai Blin, Marnix H. Medema, Daniyal Kazempour, Michael A. Fischbach, Rainer Breitling, Eriko Takano, &amp; Tilmann Weber, Nucleic Acids Research (2013), 41, W204-W212, <a href="http://dx.doi.org/10.1093/nar/gkt449">doi: 10.1093/nar/gkt449</a><br><br>
      antiSMASH: Rapid identification, annotation and analysis of secondary metabolite biosynthesis gene clusters.
Marnix H. Medema, Kai Blin, Peter Cimermancic, Victor de Jager, Piotr Zakrzewski, Michael A. Fischbach, Tilmann Weber, Rainer Breitling &amp; Eriko Takano
Nucleic Acids Research (2011), 39, W339-W346, <a href="http://dx.doi.org/10.1093/nar/gkr466">doi: 10.1093/nar/gkr466</a>.
      </div>
    </div>
    
    <script src="js/jquery.js"></script>
    <script src="js/purl.js"></script>
    <script src="js/d3.v2.js"></script>
    <script src="js/svgene.js"></script>
    <script src="js/jsdomain.js"></script>
    <script src="js/clusterblast.js"></script>
    <script src="geneclusters.js"></script>
    <script type="text/javascript">
function toggle_downloadmenu(event) {
    event.preventDefault();
    $("#downloadmenu").fadeToggle("fast", "linear");
}

function switch_to_cluster() {
    setTimeout(function() {
        var url = $.url();
        $(".page").hide();
        var anchor = url.data.attr.fragment;
        if (anchor == "") {
            anchor = "overview";
        }
        $("#" + anchor).show();
        if (geneclusters[anchor] !== undefined) {
            svgene.drawClusters(anchor+"-svg", [geneclusters[anchor]], 20, 700);
        }
        if ($("#" + anchor + "-details-svg").length > 0) {
            jsdomain.drawDomains(anchor+ "-details-svg", details_data[anchor], 40, 700);
        }
        $("#" + anchor + " .clusterblast-selector").change();
    }, 1);
}

function toggle_cluster_rules(ev) {
    ev.preventDefault();
    var id = $(this).attr('id').replace(/-header/, '');
    var rules = $('#' + id);
    if (rules.css('display') == "none") {
        $(this).text('Hide pHMM detection rules used');
    } else {
        $(this).text('Show pHMM detection rules used');
    }
    rules.fadeToggle("fast", "linear");
}

function map_type_to_desc(type) {
    switch(type) {
      case "nrps": return "NRPS";
      case "t1pks": return "Type I PKS";
      case "t2pks": return "Type II PKS";
      case "t3pks": return "Type III PKS";
      case "t4pks": return "Type IV PKS";
      default: return type;
    }
}

function copyToClipboard (text) {
    window.prompt ("Copy to clipboard: Ctrl+C, Enter", text);
}

$(document).ready(function() {

    $("#download").click(toggle_downloadmenu);

    $(".clbutton").click(function() {
        /* Make sure that even if user missed the link and clicked the
           background we still have the correct anchor */
        var href = $(this).children().first().attr('href');

        if (href === undefined) {
            return;
        }
        window.location.href = href;

        switch_to_cluster();
    }).mouseover(function() {
        /* Set the select cluster label text to cluster type */
        var classes = $(this).attr('class').split(' ');
        if (classes.length < 2) {
          return;
        }
        if (classes[1] == 'separator') {
          return;
        }
        var cluster_type = map_type_to_desc(classes[1]);
        var label = $('#cluster-type');
        label.data("orig_text", label.text());
        label.text(cluster_type + ":");
    }).mouseout(function() {
        /* and reset the select cluster label text */
        var label = $('#cluster-type');
        label.text(label.data("orig_text"));
    });

    $('.clusterblast-selector').change(function() {
        var id = $(this).attr('id').replace('-select', '');
        var url = $(this).val();
        $.get(url, function(data) {
            $('#' + id + '-svg').html(data);
            clusterblast.init(id + '-svg');
        }, 'html');
        $('#' + id + '-download').off('click');
        $('#' + id + '-download').click(function () {
            var url = $("#" + id + "-select").val();
            window.open(url, '_blank');
        });
    });

    $('.cluster-rules-header').click(toggle_cluster_rules);

    switch_to_cluster();

});
    </script>

  </body>
</html>
